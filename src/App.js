import React from 'react';
import './App.css';
import Footer from './components/Footer';
import Navbar from './components/Navbar';

function App() {
  return (
    <div className="App">
      <Navbar/>
      <div style={{height:"100vh"}}></div>
      <Footer/>
    </div>
  );
}

export default App;
