import React from "react";
import { AppBar, Toolbar, Typography } from "@mui/material";
import "./Navbar.css"

const Navbar = () => {
  return (
    <AppBar data-testid="app-bar">
      <Toolbar data-testid="tool-bar">
        <Typography
          data-testid="typography"
        //   style={{
        //     float: "none",
        //     width: "200px",
        //     marginLeft: "auto",
        //     marginRight: "auto",
        //   }}
        >
          DailyNeeds
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
