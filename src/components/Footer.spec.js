import { render, screen } from "@testing-library/react";
import Footer from "./Footer";
import React from "react";

describe("Footer", () => {
  test("renders correctly", () => {
    render(<Footer />);


    const toolbarElement = screen.getByTestId("toolbar");
    expect(toolbarElement).toBeInTheDocument();

  });
});