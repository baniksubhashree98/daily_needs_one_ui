import { render, screen } from "@testing-library/react";
import Navbar from "./Navbar";
import React from "react";

describe("Navbar", () => {
  test("renders correctly", () => {
    render(<Navbar />);
    const appBarElement = screen.getByTestId("app-bar");
    expect(appBarElement).toBeInTheDocument();

    const toolbarElement = screen.getByTestId("tool-bar");
    expect(toolbarElement).toBeInTheDocument();

    const typographyElement = screen.getByTestId("typography");
    expect(typographyElement).toBeInTheDocument();
  });
});
