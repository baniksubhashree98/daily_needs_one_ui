import React from 'react'
import useVersion from '../hooks/useVersion';
import { AppBar, Toolbar, Typography } from "@mui/material";
import "./Footer.css"

function Footer() {
    const { version } = useVersion();
  return (
    <div>
        {/* console.log({version})
        console.log({version})
        <br/>
        <br/>
        <br/> */}
        <Toolbar className="toolbar"
        data-testid="toolbar" 
        // style={{position:"static",backgroundColor:"#525252", height:"8vh",color:"white"}}
        >Current Version: {version}</Toolbar>
    </div>
  )
}

export default Footer
