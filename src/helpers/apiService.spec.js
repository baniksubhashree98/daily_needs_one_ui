import axios from "axios";
import apiService from "./apiService";

jest.mock("axios");

describe("Api service", () => {
  let internalServerErrorResponse;
  let otherErrorResponse;

  beforeEach(() => {
    // window.location.assign = jest.fn();
    Object.defineProperty(window, "location", {
      writable: true,
      value: { assign: jest.fn() },
    });

    internalServerErrorResponse = {
      response: {
        status: 500,
      },
    };

    otherErrorResponse = {
      response: {
        status: 400,
      },
    };
  });

  it("Should handle internal server error for getCurrentVersion call", async () => {
    axios.get.mockRejectedValue(internalServerErrorResponse);

    await apiService.getCurrentVersion(expect.any(String));
    expect(window.location.assign).toBeCalledTimes(1);
    expect(window.location.assign).toBeCalledWith("/error");
  });
});
