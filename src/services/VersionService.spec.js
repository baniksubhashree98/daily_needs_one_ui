import VersionService from "./VersionService";
import apiService from "../helpers/apiService";

jest.mock("../helpers/apiService");

describe("Version Service", () => {
  it("should return the current version", async () => {
    const data = {
      CurrentVersion: "v1",
    };
    apiService.getCurrentVersion.mockResolvedValue({ data: data });
    const currentVersion = await VersionService.getCurrentVersion();

    expect(currentVersion.data).toEqual({
      CurrentVersion: "v1",
    });
  });
});
