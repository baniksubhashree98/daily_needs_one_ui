import apiService from "../helpers/apiService";

export default {

    getCurrentVersion: async () => {
        const response = await apiService.getCurrentVersion("version");
        return response;
    }
}
