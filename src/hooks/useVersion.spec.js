import { renderHook } from "@testing-library/react-hooks";
import useVersion from "./useVersion";
import VersionService from "../services/VersionService";
import { when } from "jest-when";

jest.mock("../services/VersionService", () => ({
  __esModule: true,
  default: {
    getCurrentVersion: jest.fn(),
  },
}));

describe("Basic logic", () => {
  beforeEach(() => {
    when(VersionService.getCurrentVersion)
      .calledWith()
      .mockResolvedValue({data:{CurrentVersion:"v1"}});
  });

  it("Should initialize the hook with empty version", () => {
    const { result } = renderHook(() => useVersion());

    const { version } = result.current;

    expect(version).toEqual(null);
  });

  it("Should get current version after mount", async () => {
    const { result, waitForNextUpdate } = renderHook(() => useVersion());

    await waitForNextUpdate();
    const { version } = result.current;

    expect(version).toEqual("v1");
  });
});
