import { useEffect, useState } from "react";
import VersionService from "../services/VersionService";

const useVersion = () => {
  const [version, setVersion] = useState(null);

  useEffect(() => {
    VersionService.getCurrentVersion().then((response) => {
      setVersion(response.data.CurrentVersion);
    //   console.log("response.data.CurrentVersion",response.data.CurrentVersion)
      console.log("response", response)
    });
  }, []);

  return {
    version: version,
  };
};

export default useVersion;
